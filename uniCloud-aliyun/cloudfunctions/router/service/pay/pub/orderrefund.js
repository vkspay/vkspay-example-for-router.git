'use strict';
// 引入配置
const payConfig = require("../config");
// 引入工具函数
const payUtil = require("../util/payUtil");
module.exports = {
	/**
	 * 订单退款
	 * @url pay/pub/orderrefund 前端调用的url参数地址
	 * data 请求参数
	 * @param {String} params1  参数1
	 */
	main: async (event) => {
		let { data = {}, userInfo, util, filterResponse, originalParam } = event;
		let { customUtil, uniID, config, pubFun, vk, db, _ } = util;
		let { uid } = data;
		let res = { code: 0, msg: "" };
		// 业务逻辑开始-----------------------------------------------------------
		let {
			out_trade_no,
			refund_fee
		} = data;
		if (!out_trade_no) {
			return { code: -1, msg: "订单号不能为空" };
		}
		if (!refund_fee) {
			return { code: -1, msg: "退款金额不能为空" };
		}
		let key = payConfig.key;
		let requestData = {
			"method": "orderrefund",
			"merchantno": payConfig.merchantno,
			"out_trade_no": out_trade_no,
			"refund_fee": refund_fee,
			"signaturenonce": payUtil.getSignaturenonce(),
			"timestamp": payUtil.getTimestamp(),
			"version": "2.0"
		};
		// 去除值为undefined的字段
		requestData = JSON.parse(JSON.stringify(requestData));
		// 属性排序
		requestData = payUtil.objectKeySort(requestData);
		// 拼接待签名的字符串
		let signStr = payUtil.jsonToUrlString(requestData);
		// 拼接key
		signStr += `&key=${key}`;
		// 进行MD5签名
		let sign = payUtil.md5(signStr);
		// 请求参数带上签名
		requestData.sign = sign;
		console.log('requestData: ', requestData)
		console.log('sign: ', sign);
		let result = await payUtil.request({
			url: `https://vk-spay-openapi.fsq.pub/api`,
			method: "POST",
			header: {
				"content-type": "application/json; charset=utf-8",
			},
			timeout: 30000,
			data: requestData
		});
		console.log('result: ', result);

		//"out_refund_no": "R3493-20230707180825169779",
		// 业务逻辑结束-----------------------------------------------------------
		return {
			...result,
			code: 0
		};
	}
}
