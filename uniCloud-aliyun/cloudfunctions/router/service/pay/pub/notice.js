'use strict';
// 引入配置
const payConfig = require("../config");
// 引入工具函数
const payUtil = require("../util/payUtil");
module.exports = {
	/**
	 * 支付异步通知
	 * @url pay/pub/notice 前端调用的url参数地址
	 * data 请求参数
	 * @param {String} params1  参数1
	 */
	main: async (event) => {
		let { data = {}, userInfo, util, filterResponse, originalParam } = event;
		let { customUtil, uniID, config, pubFun, vk, db, _ } = util;
		let { uid } = data;
		// 业务逻辑开始-----------------------------------------------------------
		const context = originalParam.context;
		let { CLIENTIP: clientIP, SOURCE: source } = context;
		if (source !== "http") return { code: -1, msg: "请使用http请求" };
		const httpInfo = originalParam.event;
		console.log('httpInfo: ', httpInfo);
		let body = httpInfo.body;
		if (httpInfo.isBase64Encoded) {
			body = Buffer.from(body, 'base64').toString('utf-8');
		}
		if (typeof body === "string") {
			body = payUtil.urlStringToJson(body);
		}

		console.log('body: ', body)

		let {
			out_trade_no, // 订单号
		} = body;

		// 根据订单号再查一次订单是否真的支付了

		let key = payConfig.key;
		let requestData = {
			"method": "orderquery",
			"merchantno": payConfig.merchantno,
			"out_trade_no": out_trade_no,
			"signaturenonce": payUtil.getSignaturenonce(),
			"timestamp": payUtil.getTimestamp(),
			"version": "2.0"
		};
		// 去除值为undefined的字段
		requestData = JSON.parse(JSON.stringify(requestData));
		// 属性排序
		requestData = payUtil.objectKeySort(requestData);
		// 拼接待签名的字符串
		let signStr = payUtil.jsonToUrlString(requestData);
		// 拼接key
		signStr += `&key=${key}`;
		// 进行MD5签名
		let sign = payUtil.md5(signStr);
		// 请求参数带上签名
		requestData.sign = sign;
		console.log('requestData: ', requestData);
		console.log('sign: ', sign);
		let result = await payUtil.request({
			url: `https://vk-spay-openapi.fsq.pub/api`,
			method: "POST",
			header: {
				"content-type": "application/json; charset=utf-8",
			},
			timeout: 30000,
			data: requestData
		});
		console.log('result: ', result);
		if (result.return_code !== "SUCCESS") {
			console.log("------检测到异步回调疑似伪造，已拦截------");
			console.log("------检测到异步回调疑似伪造，已拦截------");
			console.log("------检测到异步回调疑似伪造，已拦截------");
			// 未支付
			// 不管未支付还是已支付，均返回 { code: "SUCCESS", msg: "SUCCESS" }
			return {
				code: "SUCCESS",
				msg: "SUCCESS"
			}
		}

		// 注意：异步回调有可能会1秒内连续回调两次，故需要自己做好防重判断，即同一个订单号只处理一次
		// 防重示例，记录全部的通知信息，方便模拟重试通知
		try {
			let setRes = await db.collection("vkspay-notice").doc(out_trade_no).set({
				httpInfo,
				notice_time: Date.now()
			});
			if (setRes.updated > 0) {
				// 代表是重复通知，直接拒绝
				console.log("------检测到重复通知，已拦截------");
				console.log("------检测到重复通知，已拦截------");
				console.log("------检测到重复通知，已拦截------");
				return {
					code: "SUCCESS",
					msg: "SUCCESS"
				}
			}

			// 已支付，执行自己的逻辑
			console.log("------已支付，执行自己的逻辑------");

			// 自己的业务逻辑开始-----------------------------------------------------------
			console.log("------自己的业务逻辑开始------");



			console.log("------自己的业务逻辑结束------");
			// 自己的业务逻辑结束-----------------------------------------------------------
		} catch (err) {
			console.error('err: ', err)
		}
		// 业务逻辑结束-----------------------------------------------------------
		return {
			code: "SUCCESS",
			msg: "SUCCESS"
		}
	}
}
