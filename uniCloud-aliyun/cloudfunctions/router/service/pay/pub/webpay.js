'use strict';
// 引入配置
const payConfig = require("../config");
// 引入工具函数
const payUtil = require("../util/payUtil");

module.exports = {
	/**
	 * 收银台支付
	 * @url pay/pub/webpay 前端调用的url参数地址
	 * data 请求参数
	 * @param {String} params1  参数1
	 */
	main: async (event) => {
		let { data = {}, userInfo, util, filterResponse, originalParam } = event;
		let { customUtil, uniID, config, pubFun, vk, db, _ } = util;
		let { uid } = data;
		let res = { code: 0, msg: "" };
		// 业务逻辑开始-----------------------------------------------------------
		let {
			total_fee
		} = data;
		if (!total_fee) {
			return { code: -1, msg: "支付金额不能为空" };
		}
		if (Number(total_fee > 0.1)) {
			return { code: -1, msg: "当前为支付试用，最大仅支持0.1元" };
		}
		let key = payConfig.key;
		// 注意：如果回调地址经常不通，会被封号，因此在未对接完异步回调时，建议可先不填异步回调地址，避免被封号
		let requestData = {
			"method": "webpay",
			"merchantno": payConfig.merchantno,
			"total_fee": total_fee,
			"notify_url": payConfig.notifyUrl,
			"signaturenonce": payUtil.getSignaturenonce(),
			"timestamp": payUtil.getTimestamp(),
			"version": "2.0",
		};
		// 去除值为undefined的字段
		requestData = JSON.parse(JSON.stringify(requestData));
		// 属性排序
		requestData = payUtil.objectKeySort(requestData);
		// 拼接待签名的字符串
		let signStr = payUtil.jsonToUrlString(requestData);
		// 拼接key
		signStr += `&key=${key}`;
		// 进行MD5签名
		let sign = payUtil.md5(signStr);
		// 请求参数带上签名
		requestData.sign = sign;
		console.log('requestData: ', requestData)
		console.log('sign: ', sign);
		let result = await payUtil.request({
			url: `https://vk-spay-openapi.fsq.pub/api`,
			method: "POST",
			header: {
				"content-type": "application/json; charset=utf-8",
			},
			timeout: 30000,
			data: requestData
		});
		console.log('result: ', result);
		// 业务逻辑结束-----------------------------------------------------------
		return {
			...result,
			code: 0
		};
	}
}
